<?php

/**
 * @file
 * Batch handlers.
 */

/**
 * Batch handler which extract entities for image rebuild process.
 *
 * @param string $entity_type
 *   Entity type.
 * @param object $context
 *   Batch context.
 */
function images_rebuild_batch_extract_entities($entity_type, &$context) {
  $entity_field_query = new EntityFieldQuery();
  $entity_field_query->entityCondition('entity_type', $entity_type);
  $entities = $entity_field_query->execute();

  $context['results']['entities'] = serialize(
    array_keys($entities[$entity_type])
  );
  $context['finished'] = 1;
}

/**
 * Batch handler which create thumbnails.
 *
 * @param string $entity_type
 *   Processed entity type.
 * @param string $style
 *   Applied image style name.
 * @param string $context
 *   Batch context.
 */
function images_rebuild_batch_do_rebuild($entity_type, $style, &$context) {
  $entities = unserialize($context['results']['entities']);

  // Prepare step.
  if (empty($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($entities);
  }

  $current_entity = entity_load_single(
    $entity_type,
    $entities[$context['sandbox']['progress']]
  );

  /** @var EntityDrupalWrapper $entity_wrapper */
  $entity_wrapper = entity_metadata_wrapper($entity_type, $current_entity);
  $context['message'] = t(
    'Applying style %style to entity %type with ID %id',
    array(
      '%type' => $entity_type,
      '%style' => $style,
      '%id' => $entity_wrapper->getIdentifier(),
    )
  );

  $processed_images_count = images_rebuild_batch_do_rebuild_process_entity(
    $current_entity,
    $entity_type,
    $style
  );

  if (!isset($context['results']['processed_images_count'])) {
    $context['results']['processed_images_count'] = 0;
  }
  $context['results']['processed_images_count'] += $processed_images_count;

  $context['sandbox']['progress'] += 1;
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Helper function which generate image thumbnail.
 *
 * @param object $entity
 *   Processed entity.
 * @param string $entity_type
 *   Entity type.
 * @param string $style_name
 *   Applied style name.
 *
 * @return int
 *   Processed images count.
 */
function images_rebuild_batch_do_rebuild_process_entity(
  $entity,
  $entity_type,
  $style_name
) {
  $image_fields = images_rebuild_get_image_fields($entity_type, $entity);

  $processed_images_count = 0;
  foreach ($image_fields as $field_name => $image_field_info) {
    $field_items = field_get_items($entity_type, $entity, $field_name);

    if (empty($field_items)) {
      return $processed_images_count;
    }

    foreach ($field_items as $field_item) {
      $destination = image_style_path($style_name, $field_item['uri']);
      if (!file_exists($destination)) {
        $style = image_style_load($style_name);
        $result = image_style_create_derivative(
          $style,
          $field_item['uri'],
          $destination
        );
        if ($result) {
          $processed_images_count += 1;
        }
      }
    }
  }
  return $processed_images_count;
}

/**
 * Finish callback for "entity prepare" batch.
 *
 * @param bool $success
 *   Batch status.
 * @param array $results
 *   Batch results.
 * @param array $operations
 *   Operations list.
 */
function images_rebuild_batch_do_rebuild_finish_callback(
  $success,
  $results,
  $operations
) {
  if ($success) {
    drupal_set_message(
      t(
        'Completed. Processed !count images',
        array('!count' => $results['processed_images_count'])
      )
    );
  }
  else {
    drupal_set_message(t('Something wrong. Try again later.'));
  }
}

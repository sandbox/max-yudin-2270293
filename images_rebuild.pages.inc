<?php

/**
 * @file
 * File contains admin pages definitions.
 */

/**
 * Configuration overview page.
 */
function images_rebuild_admin_configuration_overview_page() {
  $image_field_types = array('image');
  $instances = field_info_instances();
  $field_types = field_info_field_types();
  $bundles = field_info_bundles();

  $modules = system_rebuild_module_data();
  $header = array(t('Field name'), t('Field type'), t('Used in'), t('Actions'));
  $rows = array();
  foreach ($instances as $entity_type => $type_bundles) {
    foreach ($type_bundles as $bundle => $bundle_instances) {
      foreach ($bundle_instances as $field_name => $instance) {
        $field = field_info_field($field_name);
        if (in_array($field['type'], $image_field_types) === FALSE) {
          continue;
        }

        // Initialize the row if we encounter the field for the first time.
        if (!isset($rows[$field_name])) {
          $rows[$field_name]['class'] = $field['locked'] ? array('menu-disabled') : array('');
          $rows[$field_name]['data'][0] = $field['locked'] ? t(
            '@field_name (Locked)',
            array('@field_name' => $field_name)
          ) : $field_name;
          $module_name = $field_types[$field['type']]['module'];
          $rows[$field_name]['data'][1] = $field_types[$field['type']]['label'] . ' ' . t(
              '(module: !module)',
              array('!module' => $modules[$module_name]->info['name'])
            );
        }

        // Add the current instance.
        $admin_path = _field_ui_bundle_admin_path($entity_type, $bundle);
        $rows[$field_name]['data'][2][] = $admin_path ? l(
          $bundles[$entity_type][$bundle]['label'],
          $admin_path . '/fields'
        ) : $bundles[$entity_type][$bundle]['label'];

        // Build specific edit link.
        $rows[$field_name]['data'][3] = l(
          t('Edit'),
          $admin_path . '/fields/' . $field_name,
          array(
            'query' => array(
              'destination' => current_path(),
            ),
          )
        );
      }
    }
  }

  foreach ($rows as $field_name => $cell) {
    $rows[$field_name]['data'][2] = implode(', ', $cell['data'][2]);
  }

  $output['empty_text'] = array(
    '#markup' => t('No fields have been defined yet.'),
  );

  if (!empty($rows)) {
    // Sort rows by field name.
    ksort($rows);
    $output = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
  }

  return $output;
}

/**
 * Form which allow to rebuild all thumbnails for selected entities.
 */
function images_rebuild_admin_do_rebuild($form, &$form_state) {
  // Build entity types list.
  $entity_types = entity_get_info();
  $options_entity_types = array();
  foreach ($entity_types as $id => $entity_type) {
    $options_entity_types[$id] = $entity_type['label'];
  }

  $form['entity_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entity types'),
    '#required' => TRUE,
    '#description' => t('Select which entity types should be rebuilt.'),
    '#options' => $options_entity_types,
  );

  // Build styles list.
  $available_image_styles = image_styles();
  $styles_options = array();
  foreach ($available_image_styles as $id => $style) {
    $styles_options[$id] = $style['label'];
  }

  $form['styles'] = array(
    '#title' => t('Image styles for flush'),
    '#description' => t('Flush selected image styles before images rebuild.'),
    '#type' => 'checkboxes',
    '#options' => $styles_options,
    '#default_value' => array_keys($styles_options),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Start images rebuild'),
  );

  return $form;
}

/**
 * Build batch which collect images from nodes.
 */
function images_rebuild_admin_do_rebuild_submit($form, &$form_state) {
  // Flush image styles.
  $styles = image_styles();
  foreach ($form_state['values']['styles'] as $id => $is_selected) {
    if ($is_selected) {
      image_style_flush($styles[$id]);
    }
  }

  $operations = array();
  foreach ($form_state['values']['entity_types'] as $entity_type) {
    if (!empty($entity_type)) {
      $operations[] = array(
        'images_rebuild_batch_extract_entities',
        array($entity_type),
      );
    }
    foreach ($form_state['values']['styles'] as $style) {
      if (!empty($entity_type) && !empty($style)) {
        $operations[] = array(
          'images_rebuild_batch_do_rebuild',
          array(
            $entity_type,
            $style,
          ),
        );
      }
    }
  }

  $batch = array(
    'title' => t('Prepare images rebuild thumbnails'),
    'operations' => $operations,
    'finished' => 'images_rebuild_batch_do_rebuild_finish_callback',
    'progress_message' => t('Prepared materials: @current / @total.'),
    'file' => sprintf(
      '%s/images_rebuild.batch.inc',
      drupal_get_path('module', 'images_rebuild')
    ),
  );

  batch_set($batch);
}
